# Python program to find the operating system name, platform and platform release date #
import os, platform
print("Operating system name:", os.name)

print("Platform name:", platform.system())

print("Platform release:", platform.release())

